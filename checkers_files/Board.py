import string
from tabulate import tabulate

class Board:
    def __init__(self, rows, columns) -> None:
        self.rows = rows
        self.columns = columns
        self.board = []
        self.run = True

    def create_board(self):
        for i in range(self.rows):
            row = []
            for j in range(self.columns):
                if i != 3 and i != 4:
                    if i < 3:
                        if i % 2 != 0:
                            if j % 2 == 0:
                                row.append('X')
                            else:
                                row.append(' ')
                        else:
                            if j % 2 == 0:
                                row.append(' ')
                            else:
                                row.append('X')
                    if i > 4:
                        if i % 2 != 0:
                            if j % 2 == 0:
                                row.append('O')
                            else:
                                row.append(' ')
                        else:
                            if j % 2 == 0:
                                row.append(' ')
                            else:
                                row.append('O')
                else:
                    row.append(' ')
            self.board.append(row)

    def print_board(self):
        headers = [''] + list(string.ascii_uppercase[:self.columns])
        rows = [[str(i + 1)] + row for i, row in enumerate(self.board)]
        print(tabulate(rows, headers=headers, tablefmt='grid'))

    def check_possible_moves(self,sign):
        result_dict = {}

        for i, row in enumerate(self.board):
            for j, element in enumerate(row):
                if sign == element == 'O':
                    if i-1 >= 0 and j-1 >= 0 and self.board[i-1][j-1] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j - 1 + ord('a')).upper()
                        if char.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i}{place}')

                    if i-1 >= 0 and j + 1 < len(row) and self.board[i-1][j+1] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j + 1 + ord('a')).upper()
                        if char.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i}{place}')

                if sign == element == 'X':
                    if i + 1 < len(self.board) and j + 1 < len(row) and self.board[i+1][j+1] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j + 1 + ord('a')).upper()
                        if char.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i + 2}{place}')

                    if i + 1 < len(self.board) and j - 1 >= 0 and self.board[i+1][j-1] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j - 1 + ord('a')).upper()
                        if char.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i + 2}{place}')

        if result_dict:
            return result_dict
        else:
            print('remis! koniec gry!')
            self.run = False


    def move_piece(self, start, end):
        start_i, start_j = start
        end_i, end_j = end
        self.board[end_i][end_j] = self.board[start_i][start_j]
        self.board[start_i][start_j] = ' '

    def move_piece_to_beat(self, start, end):
        start_i, start_j = start
        end_i, end_j = end
        self.board[end_i][end_j] = self.board[start_i][start_j]
        self.board[start_i][start_j] = ' '
        self.board[(end_i-((end_i-start_i)//2))][(end_j-((end_j-start_j)//2))] = ' '

    def check_possible_beatings(self,sign):
        result_dict = {}
        anti_sign = ''
        if sign == 'O':
            anti_sign = 'X'
        else:
            anti_sign = 'O'
        for i, row in enumerate(self.board):
            for j, element in enumerate(row):
                if  element == sign:
                    if i-2 >= 0 and j-2 >= 0 and self.board[i-1][j-1] == anti_sign and  self.board[i-2][j-2] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j - 2 + ord('a')).upper()
                        if place.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i-1}{place}')

                    if i-2 >= 0 and j+2 < len(row) and self.board[i-1][j+1] == anti_sign and  self.board[i-2][j+2] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j + 2 + ord('a')).upper()
                        if place.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i-1}{place}')

                    if i+2 < len(self.board) and j+2 < len(row) and self.board[i+1][j+1] == anti_sign and  self.board[i+2][j+2] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j + 2 + ord('a')).upper()
                        if place.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i+3}{place}')

                    if i+2 < len(self.board) and j -2 >=0 and self.board[i+1][j-1] == anti_sign and  self.board[i+2][j-2] == ' ':
                        char = chr(j + ord('a')).upper()
                        piece = f'{i+1}{char}'
                        if piece not in result_dict:
                            result_dict[piece] = []
                        place = chr(j - 2 + ord('a')).upper()
                        if place.isalpha() and place.isupper():
                            result_dict[piece].append(f'{i+3}{place}')

        if result_dict:
            return result_dict
        else:
            self.run = False
