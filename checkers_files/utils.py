import re
def check_index(index):
    pattern = r'^[1-8][A-H]$'
    return True if re.match(pattern, index) else False

def convert_coordinates_to_string(piece):
    return str(piece[0] + 1) + chr(piece[1] + ord('a')).upper()

def show_possible_moves(result_dict):
    for piece, moves in result_dict.items():
                print(f"Z {piece} możesz ruszyć na: {', '.join(moves)}")

def check_who_is_winner(board, sign):
    return any(sign in row for row in board)
