from checkers_files.Player import Player
import random

class Computer():
    sign = 'X'
    def __init__(self, board):
        self.board = board

    def check_possible_moves(self):
        return self.board.check_possible_moves(self.sign)
    def check_possible_beatings(self):
        return self.board.check_possible_beatings(self.sign)

    def random_move(self):
        all_moves = self.check_possible_moves()
        if all_moves:
            random_piece = random.choice(list(all_moves.keys()))
            random_location = random.choice(all_moves[random_piece])
            start = Player.position_on_index(random_piece)
            end = Player.position_on_index(random_location)
            self.board.move_piece(start, end)
        else:
            self.board.run = False

    def random_beat(self):
        all_beats = self.check_possible_beatings()

        if all_beats:
            random_piece = random.choice(list(all_beats.keys()))
            random_location = random.choice(all_beats[random_piece])
            start = Player.position_on_index(random_piece)
            end = Player.position_on_index(random_location)
            self.board.move_piece_to_beat(start, end)