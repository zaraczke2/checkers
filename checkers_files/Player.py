from checkers_files import utils

class Player:
    sign = 'O'
    def __init__(self, board):
        self.board = board

    def check_possible_moves(self):
        return self.board.check_possible_moves(self.sign)
    def check_possible_beatings(self):
        return self.board.check_possible_beatings(self.sign)

    def choose_index_of_piece(self, prompt):
        available_pieces = list(self.check_possible_moves().keys())
        position = input(prompt)
        index = self.position_on_index(position)
        indexTmp = utils.convert_coordinates_to_string(index)
        if utils.check_index(indexTmp):
            if indexTmp not in available_pieces:
                print("Nie możesz ruszyć tym pionkiem")
            else:
                return index

    def choose_index_of_piece_to_beat(self, prompt):
        available_pieces = list(self.check_possible_beatings().keys())
        position = input(prompt)
        index = self.position_on_index(position)
        indexTmp = utils.convert_coordinates_to_string(index)
        if utils.check_index(indexTmp):
            if indexTmp not in available_pieces:
                print("Nie możesz ruszyć tym pionkiem")
            else:
                return index

    def choose_index_of_place(self, prompt, start_position):
        all_moves = self.check_possible_moves()
        start_position = utils.convert_coordinates_to_string(start_position)
        start_moves = all_moves.get(start_position, [])
        position = input(prompt)
        index = self.position_on_index(position)
        indexTmp = utils.convert_coordinates_to_string(index)
        if utils.check_index(indexTmp):
            if indexTmp in start_moves:
                return index
            else:
                print("Nie możesz tutaj ruszyć")
        else:
            print("Nieprawidłowe współrzędne")

    def choose_index_of_place_after_beat(self, prompt, start_position):
        all_moves = self.check_possible_beatings()
        start_position = utils.convert_coordinates_to_string(start_position)
        start_moves = all_moves.get(start_position, [])
        position = input(prompt)
    #    if not position[0].isdigit() or position[1].isalpha():
    #         print("Nieprawidłowe współrzędne")
    #     else: posprawdzać to dobrze
        index = self.position_on_index(position)
        indexTmp = utils.convert_coordinates_to_string(index)
        if utils.check_index(indexTmp):
            if indexTmp in start_moves:
                return index
            else:
                print("Nie możesz tutaj ruszyć")
        else:
            print("Nieprawidłowe współrzędne")

    @staticmethod
    def position_on_index(position):
        digit, char = position[0], position[1]
        char_index = ord(str(char).lower()) - ord('a')
        digit_index = int(digit) - 1
        return digit_index, int(char_index)