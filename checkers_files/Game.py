from checkers_files.Board import Board
from checkers_files import constants
from checkers_files.Player import Player
from checkers_files.Computer import Computer
from checkers_files.utils import check_who_is_winner
import time
import os


class Game:
    def __init__(self) -> None:
        self.checkers = Board(constants.ROWS, constants.COLUMNS)
        self.player = Player(self.checkers)
        self.computer = Computer(self.checkers)
        self.run = self.checkers.run

    def play(self):
        if not self.checkers.check_possible_beatings(Player.sign):
            start_position = self.player.choose_index_of_piece("Podaj współrzędne pionka: ")
            if start_position:
                end_position = self.player.choose_index_of_place("Podaj miejsce, gdzie chcesz przenieść pionka: ", start_position)
                if end_position:
                    self.checkers.move_piece(start_position, end_position)
        else:
            while self.checkers.check_possible_beatings(Player.sign):
                print("Musisz bić")
                start_position = self.player.choose_index_of_piece_to_beat("Podaj współrzędne pionka do bicia: ")
                if start_position:
                    end_position = self.player.choose_index_of_place_after_beat("Podaj miejsce, gdzie staniesz po biciu: ", start_position)
                    if end_position:
                        self.checkers.move_piece_to_beat(start_position, end_position)
                        os.system('cls')
                        self.checkers.print_board()
        time.sleep(0.8)
        if not self.checkers.check_possible_beatings(Computer.sign):
            self.computer.random_move()
        else:
            while self.checkers.check_possible_beatings(Computer.sign):
                self.computer.random_beat()
                time.sleep(0.8)
                os.system('cls')
                self.checkers.print_board()


    def start_game(self):
        self.checkers.create_board()

        while self.run:
            self.checkers.print_board()
            if not check_who_is_winner(self.checkers.board, 'X'):
                print('Wygrałeś !')
                break
            if not check_who_is_winner(self.checkers.board, 'O'):
                print('Przegrałeś !')
                break
            self.play()
            os.system('cls')